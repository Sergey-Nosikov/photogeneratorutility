﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;

namespace GeneratorFakeData
{
	class Program
	{
		static void Main(string[] args)
		{
			var numberOfPhotos = int.Parse(ConfigurationManager.AppSettings["NumberOfPhotos"]);
			var apiUri = new Uri(ConfigurationManager.AppSettings["ApiUri"]);

			var app = new App(apiUri, numberOfPhotos);
			app.Start();
		}
	}
}
