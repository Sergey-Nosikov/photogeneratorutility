using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace AmInTech.Core
{
	public class ApiRequest
	{
		private readonly WebRequest _request;
		private string _boundary;
		private bool _isMultipart;
		private string _body;
		private readonly Dictionary<string, string> _fields;
		private readonly List<Attachment> _attachments;
		private readonly JsonSerializerSettings _serializerSettings;
		private ICredentials _credentials;

		public ApiRequest(string method, string endpoint, JsonSerializerSettings serializerSettings)
		{
			_serializerSettings = serializerSettings;
			_request = WebRequest.CreateHttp(endpoint);
			_request.Method = method;

			_isMultipart = false;
			_fields = new Dictionary<string, string>();
			_attachments = new List<Attachment>();
		}

		public ApiRequest SetProxy(IWebProxy proxy)
		{
			_request.Proxy = proxy;
			return this;
		}

		public ApiRequest UseCredentials(ICredentials credentials)
		{
			_credentials = credentials;

			return this;
		}

		public ApiRequest SetBody(string body)
		{
			_body = body;

			return this;
		}

		public ApiRequest SetJsonBody<T>(T obj)
		{
			_request.ContentType = "application/json";

			_body = JsonConvert.SerializeObject(obj, _serializerSettings);

			return this;
		}

		public ApiRequest ClearBody()
		{
			_body = null;

			return this;
		}

		public ApiRequest AttachFile(string fieldName, string fileName, string metaType, Stream stream)
		{
			_isMultipart = true;
			_attachments.Add(new Attachment
			{
				FieldName = fieldName,
				FileName = fileName,
				MimeType = metaType,
				Stream = stream,
			});

			return this;
		}

		public ApiRequest AddField(string fieldName, string value)
		{
			_fields.Add(fieldName, value);

			return this;
		}

		public WebResponse WebResponse()
		{
			if (_credentials != null)
			{
				var credential = _credentials.GetCredential(_request.RequestUri, "Basic");
				var encoded = Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(credential.UserName + ":" + credential.Password));
				_request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + encoded);
			}

			if (_request.Method == WebRequestMethods.Http.Post || _request.Method == WebRequestMethods.Http.Put)
			{
				WriteBody();
			}

			return _request.GetResponse();
		}

		public string Response()
		{
			using var webResponse = WebResponse();
			using var responseStream = webResponse.GetResponseStream();
			using var streamReader = new StreamReader(responseStream!);

			return streamReader.ReadToEnd();
		}

		public T Response<T>()
		{
			return JsonConvert.DeserializeObject<T>(Response(), _serializerSettings);
		}

		private void WriteBody()
		{
			var stream = _request.GetRequestStream();
			using var streamWriter = new StreamWriter(stream);
			if (_body != null)
			{
				streamWriter.Write(_body);
				return;
			}

			if (_isMultipart)
			{
				_request.ContentType = "multipart/form-data; boundary=" + GetBoundary();
				foreach (var (key, value) in _fields)
				{
					streamWriter.WriteLine($"--{GetBoundary()}");
					streamWriter.WriteLine($"Content-Disposition: form-data; name=\"{key}\"");
					streamWriter.WriteLine();
					streamWriter.WriteLine(value);
				}

				foreach (var attach in _attachments)
				{
					streamWriter.WriteLine($"--{GetBoundary()}");
					streamWriter.WriteLine($"Content-Disposition: form-data; name=\"{attach.FieldName}\"; filename=\"{attach.FileName}\"");
					streamWriter.WriteLine($"Content-Type: {attach.MimeType}");
					streamWriter.WriteLine();
					streamWriter.Flush();
					attach.Stream.CopyTo(stream);
				}

				streamWriter.Write("\r\n--" + GetBoundary() + "--\r\n");
				streamWriter.Flush();
			}
		}

		private string GetBoundary()
		{
			if (string.IsNullOrEmpty(_boundary))
			{
				_boundary = Guid.NewGuid().ToString();
			}

			return _boundary;
		}

		private class Attachment
		{
			public string FieldName { get; set; }
			public string FileName { get; set; }
			public string MimeType { get; set; }
			public Stream Stream { get; set; }
		}
	}
}
