﻿using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace AmInTech.Core
{
    public abstract class ApiServiceBase
    {
        private readonly ICredentials _credentials;
        protected JsonSerializerSettings SerializerSettings;

        public string BaseUrl { get; }

        protected ApiServiceBase(string baseUrl, ICredentials credentials = null)
        {
            BaseUrl = baseUrl;
            _credentials = credentials;
        }

        public ApiRequest CreateRequest(string method, string endpoint)
        {
            var request = new ApiRequest(method, BaseUrl + endpoint, SerializerSettings);
            if (_credentials != null)
            {
                request.UseCredentials(_credentials);
            }

            return request;
        }

        public ApiRequest CreateGet(string endpoint)
        {
            return CreateRequest(WebRequestMethods.Http.Get, endpoint);
        }

        public string Get(string endpoint)
        {
            return CreateGet(endpoint).Response();
        }

        public T Get<T>(string endpoint)
        {
            return JsonConvert.DeserializeObject<T>(Get(endpoint));
        }

        public ApiRequest CreatePost(string endpoint)
        {
            return CreateRequest(WebRequestMethods.Http.Post, endpoint);
        }

        public string Post(string endpoint, string body)
        {
            return CreatePost(endpoint).SetBody(body).Response();
        }

        public T Post<T>(string endpoint, string body)
        {
            return JsonConvert.DeserializeObject<T>(Post(endpoint, body));
        }

        public T Post<T>(string endpoint, object obj)
        {
            return Post<T>(endpoint, JsonConvert.SerializeObject(obj));
        }

        protected static void AttachFile(Stream requestStream, string fieldName, Stream stream)
        {
            stream.CopyTo(requestStream);
        }
    }
}
