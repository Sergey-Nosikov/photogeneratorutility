﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GeneratorFakeData.Services
{
	class SignatureCheckerService
	{
		private readonly byte[] jpgSignature = new byte[] { 0xFF, 0xD8, 0xFF };

		public bool CheckJpeg(Stream photo)
		{
			photo.Position = 0;

			var isJpg = CheckJpgSignature(photo);

			photo.Position = 0;

			return isJpg;
		}

		private bool CheckJpgSignature(Stream stream)
		{
			if (stream.Length < 4) return false;
			for (int i = 0; i < jpgSignature.Length; i++)
				if (jpgSignature[i] != stream.ReadByte())
					return false;

			return true;
		}
	}
}
