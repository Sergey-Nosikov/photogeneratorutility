﻿using AmInTech.Core;
using GeneratorFakeData.Services.ProxyListService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace GeneratorFakeData.Services
{
	class PhotoRecipientService : ApiServiceBase
	{
		private ProxyQueueService _proxyQueueService { get; }
		public PhotoRecipientService(ProxyQueueService proxyService) : base("https://thispersondoesnotexist.com")
		{
			_proxyQueueService = proxyService;
		}

		public Stream GetPhoto()
		{
			var responseStream = CreateGet("/image")
				.SetProxy(_proxyQueueService.GetProxy())
				.WebResponse()
				.GetResponseStream();
			return ToMemoryStream(responseStream);
		}

		private MemoryStream ToMemoryStream(Stream stream)
		{
			var result = new MemoryStream();
			stream.CopyTo(result);
			return result;
		}
	}
}