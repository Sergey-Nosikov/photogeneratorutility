﻿using GeneratorFakeData.Services.ProxyListService;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Security.Permissions;
using System.Text;

namespace GeneratorFakeData.Services.ProxyListService
{
	class ProxyQueueService
	{
		private ConcurrentQueue<IWebProxy> proxyQueue { get; set; }
		private IProxyRecipient _proxyRecipient { get; }
		public bool isInitialized { get; private set; } = false;

		public ProxyQueueService(IProxyRecipient proxyRecipient)
		{
			_proxyRecipient = proxyRecipient;
		}

		public void Initialize()
		{
			proxyQueue = new ConcurrentQueue<IWebProxy>(_proxyRecipient.TakeProxyList());
			isInitialized = true;
		}

		public IWebProxy GetProxy()
		{
			IWebProxy proxy;
			if (!proxyQueue.TryDequeue(out proxy))
				throw new Exception();

			proxyQueue.Enqueue(proxy);
			return proxy;
		}
	}

}
