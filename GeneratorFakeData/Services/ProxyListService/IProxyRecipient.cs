﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace GeneratorFakeData.Services.ProxyListService
{
	interface IProxyRecipient
	{
		public IEnumerable<IWebProxy> TakeProxyList();
	}
}
