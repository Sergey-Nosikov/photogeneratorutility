﻿using AmInTech.Core;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;

namespace GeneratorFakeData.Services.ProxyListService
{
	class ProxyRecipientFromApi : ApiServiceBase, IProxyRecipient
	{
		public ProxyRecipientFromApi() : base("http://172.16.252.5:5195/")
		{

		}

		public IEnumerable<IWebProxy> TakeProxyList()
		{
			return CreateGet("/api/v1/proxy/valid")
				.Response<WebProxyItem[]>()
				.Select(i =>
				{
					var credentials = new NetworkCredential(i.User, i.Password);
					var bypassList = new string[] { };
					var proxy = new WebProxy
					{
						Address = new Uri($"http://{i.Address}"),
						Credentials = new NetworkCredential(i.User, i.Password)
					};
					
					return proxy;
				});

		}
		private class WebProxyItem
		{
			public string Address { get; set; }
			public string User { get; set; }
			public string Password { get; set; }
		}
	}
}
