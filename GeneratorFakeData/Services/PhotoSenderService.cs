﻿using AmInTech.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;

namespace GeneratorFakeData.Services
{
	internal class PhotoSenderService : ApiServiceBase
	{
		public PhotoSenderService(Uri uri) : base($"{uri.Scheme}://{uri.Authority}") { }
		public void SendPhoto(Stream photo) => CreatePost("/v1/photo")
			.AddField("exteralId", "")
			.AddField("timestamp", DateTime.Now.ToString())
			.AttachFile("photo", "photo.jpg", "image/jpeg", photo)
			.WebResponse();
	}
}
