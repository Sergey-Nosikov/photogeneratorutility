﻿using AmInTech.Core;
using GeneratorFakeData.Services;
using GeneratorFakeData.Services.ProxyListService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorFakeData
{
	class App
	{
		private PhotoRecipientService photoGetter { get; }
		private PhotoSenderService photoSender { get; }
		private SignatureCheckerService photoChecker { get; }
		private ProxyQueueService proxyQueueService { get; }


		public int Count { get; set; }
		public App(Uri api, int count = 1)
		{
			Count = count;

			var proxyRecipient = new ProxyRecipientFromApi();
			proxyQueueService = new ProxyQueueService(proxyRecipient);

			proxyQueueService.Initialize();

			photoGetter = new PhotoRecipientService(proxyQueueService);
			photoSender = new PhotoSenderService(api);
			photoChecker = new SignatureCheckerService();
		}

		public void Start()
		{
			Parallel.For(0, Count, i =>
			{
				using (var photo = photoGetter.GetPhoto())
				{
					if (!photoChecker.CheckJpeg(photo)) throw new Exception("Photo is't jpeg");

					photoSender.SendPhoto(photo);
				}
			});

			Debug.WriteLine("Done");
		}
	}
}
